var express = require('express');
var router = express.Router();

const movies=[
  {
    id:1,
    movie:"Interstellar"
  },

  {
    id:2,
    movie:"Taxi Driver"
  },

  {
    id:3,
    movie:"V For Vendetta"
  },

  {
    id:4,
    movie:"Un Poisson Nommé Wanda"
  }
];

/* GET Movies listing. */
router.get('/', (req, res) => {
  // Get List of user and return JSON
  res.status(200).json({ movies });
});

/* GET one movie. */
router.get('/:id', (req, res) => {
  const  idSearch = req.params;
  // Find user in DB
  const movie = movies.find(m => m.id === parseInt(idSearch.id));
  // Return user
  res.status(200).json({movie});
});

/* PUT new movie. */
router.put('/', (req, res) => {
  // Get the data from request from request
  const { movie } = req.body;
  // Create new unique id
  const id = movies.length+1;
  // Insert it in array (normaly with connect the data with the database)
  movies.push({ movie, id });
  // Return message
  res.json({
    message: `Just added ${id}`,
    movie: { movie, id }
  });
});

/* UPDATE movie. */
router.post('/:id', (req, res) => {
  // Get the :id of the movie we want to update from the params of the request
  const { id } = req.params;
  // Get the new data of the movie we want to update from the body of the request
  const { movie } = req.body;
  // Find in DB
  const movieToUpdate = movies.find(m => m.id === parseInt(idSearch.id));
  // Update data with new data (js is by address)
  movies.splice(id-1, 1);
  movies.push(movieToUpdate);

  // Return message
  res.json({
    message: `Just updated ${id} with ${movie}`
  });
});

/* DELETE movie. */
router.delete('/:id', (req, res) => {
  // Get the :id of the movie we want to delete from the params of the request
  const { id } = req.params;

  // Remove from "DB"
  movies.splice(id-1, 1);

  // Return message
  res.json({
    message: `Just removed ${id}`
  });
});

module.exports = router;